#include <stdio.h>
#include <stdlib.h>

// Node structure
struct Node
{
    int data;
    struct Node *next;
};

struct Node *createNode(int num);

void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

// Function to create a new node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Error:Allocation of memory for a new node failed\n");
        exit(1);
    }

    newNode->data = num;
    newNode->next = NULL;
    return newNode;
}

// Function to print the linked list
void printList(struct Node *head){
    struct Node *current = head;
    printf("[");
    while (current != NULL){
        printf("%d", current->data);
        current = current->next;

        if (current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to append a node to the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL){
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL){
        current = current->next;
    }
    current->next = newNode;
}

// Function to prepend a node to the beginning of the list
void prepend(struct Node **head, int num){
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key){
    if (*head == NULL)
        return;

    struct Node *temp = *head;
    struct Node *prev = NULL;

    if (temp != NULL && temp->data == key){
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->data != key){
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    prev->next = temp->next;
    free(temp);
}

// Function to delete a node by value
void deleteByValue(struct Node **head, int value){
    if (*head == NULL)
        return;

    struct Node *temp = *head;
    struct Node *prev = NULL;

    if (temp != NULL && temp->data == value){
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->data != value){
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    prev->next = temp->next;
    free(temp);
}

// Function to insert a node after a node with a given key
void insertAfterKey(struct Node **head, int key, int value){
    if (*head == NULL)
        return;

    struct Node *temp = *head;

    while (temp != NULL && temp->data != key){
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Function to insert a node after a node with a given value
void insertAfterValue(struct Node **head, int searchValue, int newValue){
    if (*head == NULL)
        return;

    struct Node *temp = *head;

    while (temp != NULL && temp->data != searchValue)
{
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Driver program
int main()
{
    struct Node *head = NULL;
    int choice, data, key, searchValue, newValue;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;  	
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
            break;
        case 6:
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &data);
            insertAfterKey(&head, key, data);
            break;
        case 7:
            printf("Enter value after which to insert: ");
            scanf("%d", &searchValue);
            printf("Enter value to insert: ");
            scanf("%d", &newValue);
            insertAfterValue(&head, searchValue, newValue);
            break;
        case 8:
            printf("Exiting program.\n");
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
            break;
        }
    }

    return 0;
}




































 