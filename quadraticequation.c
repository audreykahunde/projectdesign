#include<stdio.h>
#include<math.h>

int calcRoots(int a, int b, int c);
int main()
{
    int a, b, c;
    printf("Enter a: ");
    scanf("%d", &a);
    printf("Enter b: ");
    scanf("%d", &b);
    printf("Enter c: ");
    scanf("%d", &c);
    if (a == 0){
        printf("Invalid input");
        return 1;
    }
    calcRoots(a,b,c);
    return 0;
}
// (-b +- sqrt(discriminant))/2a
int calcRoots(int a, int b, int c){
    int discriminant = b * b - 4 * a * c;

    if(discriminant == 0){
        double root = (-b/(2.0*a));
        printf("Real and Equal roots\nRoot: %.4f",root);
    }
    else if(discriminant > 0){
        double root1 = (-b + sqrt(discriminant)/(2.0*a));
        double root2 = (-b - sqrt(discriminant)/(2.0*a));
        printf("Real and Distinct roots\nRoot 1: %.4f\nRoot 2: %.4f",root1,root2);
    }
    else{
        printf("Complex roots");
    }
    return 0;
}

